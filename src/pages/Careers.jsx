import React from "react";
import Template from "../components/Template";

const Careers = () => {
  return (
    <Template>
      <div className="h-[100vh] flex justify-center items-center w-full">
        Careers
      </div>
    </Template>
  );
};

export default Careers;
