const Pagination = ({ totalPages, currentPage, setCurrentPage }) => {
  return (
    <div className="flex justify-center items-center py-6">
      <button>
        {currentPage > 1 && (
          <button
            className="p-2 rounded shadow"
            onClick={() => setCurrentPage(currentPage - 1)}
          >
            Prev
          </button>
        )}
        <button className="p-2 mx-2 rounded shadow">{currentPage}</button>
        {currentPage < totalPages && (
          <button
            className="p-2 rounded shadow"
            onClick={() => setCurrentPage(currentPage + 1)}
          >
            Next
          </button>
        )}
      </button>
    </div>
  );
};

export default Pagination;
