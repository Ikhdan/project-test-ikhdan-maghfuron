import React from "react";
import Template from "../components/Template";

const Services = () => {
  return (
    <Template>
      <div className="h-[100vh] flex justify-center items-center w-full">
        Services
      </div>
    </Template>
  );
};

export default Services;
