import Work from "./Work";
import Ideas from "./Ideas";
import About from "./About";
import Careers from "./Careers";
import Services from "./Services";
import Contact from "./Contact";
export { Work, Ideas, Services, Contact, Careers, About };
