import axios from "axios";
import { useEffect, useState } from "react";
import Header from "./components/Header";
import Navbar from "./components/Navbar";
import Pagination from "./components/Pagination";
import SkeletonLoader from "./components/Skeleton";

function App() {
  const [dataList, setDataList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [selected, setSelected] = useState(10);
  const [sort, setSort] = useState("-published_at");
  const totalPages = 30 / selected;
  async function getData(num, item, times) {
    setLoading(true);
    const response = await axios.get(
      `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${num}&page[size]=${item}&sort=${times}`
    );
    setLoading(false);
    setDataList(response.data.data);
  }

  console.log(dataList);

  const handleChange = (event) => {
    setSelected(event.target.value);
  };
  const handleSort = (event) => {
    setSort(event.target.value);
  };

  const seconds = currentPage * selected > 30 ? 30 : currentPage * selected;
  const start = currentPage == 2 ? selected : 30 - selected;

  useEffect(() => {
    getData(currentPage, selected, sort);
  }, [currentPage, selected, sort]);
  return (
    <>
      <Navbar />
      <Header />

      <div className="p-12">
        <div className="flex w-full flex-wrap justify-between">
          <div className="w-full flex flex-wrap mb-2 justify-between">
            <div className="w-full  my-2 lg:w-1/2">
              <p>
                showing {currentPage == 1 ? currentPage : start}-{seconds} of 30
              </p>
            </div>
            <div className="w-full lg:w-1/2 flex justify-between">
              <div className="flex items-center">
                <p className="mr-2">show per page</p>
                <select
                  value={selected}
                  onChange={handleChange}
                  className="shadow-lg border"
                >
                  <option value="10">10</option>
                  <option value="20">20</option>
                  <option value="50">50</option>
                </select>
              </div>
              <div className="flex items-center">
                <p className="mr-2">show by</p>
                <select
                  onChange={handleSort}
                  value={sort}
                  className="shadow-lg border"
                >
                  <option value="-published_at">Newest</option>
                  <option value="published_at">Oldest</option>
                </select>
              </div>
            </div>
          </div>

          {!loading ? (
            dataList.map((data) => (
              <div
                key={data.id}
                className="shadow-lg w-[49%] lg:w-[24%] rounded-lg my-2"
              >
                <img src="suitmedia.png" className="rounded-lg" alt="" />
                <div className="p-6">
                  <span className="text-gra-500">{data.published_at}</span>
                  <h2 className="font-semibold line-clamp-3">{data.title}</h2>
                </div>
              </div>
            ))
          ) : (
            <SkeletonLoader />
          )}
        </div>
        <Pagination
          setCurrentPage={setCurrentPage}
          currentPage={currentPage}
          totalPages={totalPages}
        />
      </div>
    </>
  );
}

export default App;
