import React from "react";

const Header = () => {
  return (
    <div className="flex w-full h-[60vh]  w-full justify-center items-center bg-bottom text-white bg-fixed  bg-[url('/header.jpeg')] bg-cover">
      <div className="text-center">
        <h1 className="text-3xl">Ideas</h1>
        <p>where all our great things begin</p>
      </div>
    </div>
  );
};

export default Header;
