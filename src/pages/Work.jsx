import React from "react";
import Template from "../components/Template";

const Work = () => {
  return (
    <Template>
      <div className="h-[100vh] flex justify-center items-center w-full">
        Work
      </div>
    </Template>
  );
};

export default Work;
