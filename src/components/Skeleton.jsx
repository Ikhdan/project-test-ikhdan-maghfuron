import React from "react";

const Skeleton = () => {
  return (
    <div className="w-[49%] lg:w-[24%] shadow-lg my-2 rounded-lg bg-gray-200 border animate-pulse rounded">
      <div className="h-[150px] lg:h-[300px] bg-gray-600 rounded"></div>
      <div className="p-4">
        <p className="w-1/2 h-[11px] my-4 bg-gray-600 "></p>
        <p className="w-full h-[16px] mb-2 bg-gray-600 "></p>
        <p className="w-full h-[16px] bg-gray-600 "></p>
      </div>
    </div>
  );
};

const SkeletonLoader = () => {
  return (
    <>
      <Skeleton />
      <Skeleton />
      <Skeleton />
      <Skeleton />
      <Skeleton />
      <Skeleton />
      <Skeleton />
      <Skeleton />
    </>
  );
};

export default SkeletonLoader;
