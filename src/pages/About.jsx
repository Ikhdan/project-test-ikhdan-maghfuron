import React from "react";
import Template from "../components/Template";

const About = () => {
  return (
    <Template>
      <div className="h-[100vh] flex justify-center items-center w-full">
        About
      </div>
    </Template>
  );
};

export default About;
