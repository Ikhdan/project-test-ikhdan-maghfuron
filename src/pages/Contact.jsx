import React from "react";
import Template from "../components/Template";

const Contact = () => {
  return (
    <Template>
      <div className="h-[100vh] flex justify-center items-center w-full">
        Contact
      </div>
    </Template>
  );
};

export default Contact;
