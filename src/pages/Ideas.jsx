import React from "react";
import Template from "../components/Template";

const Ideas = () => {
  return (
    <Template>
      <div className="h-[100vh] flex justify-center items-center w-full">
        Ideas
      </div>
    </Template>
  );
};

export default Ideas;
