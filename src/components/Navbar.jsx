import React from "react";
import { useState } from "react";
import { Link, useLocation } from "react-router-dom";

const Navbar = () => {
  const { pathname } = useLocation();
  const [slide, setSlide] = useState(false);
  const navlists = [
    {
      path: "/work",
      name: "work",
    },
    {
      path: "/about",
      name: "about",
    },
    {
      path: "/services",
      name: "services",
    },
    {
      path: "/ideas",
      name: "ideas",
    },
    {
      path: "/careers",
      name: "careers",
    },
    {
      path: "/contact",
      name: "contact",
    },
  ];
  return (
    <div className="fixed top-0 w-full px-12 bg-[#ff6600] text-white">
      <nav className="flex flex-wrap justify-between items-center">
        <div className="w-full lg:w-2/3 flex justify-between items-center">
          <Link to="/">
            <img src="suitmedia.png" alt="" height="60px" width="80px" />
          </Link>
          <div
            className="w-[25px] h-[25px] flex lg:hidden flex-col justify-between"
            onClick={() => setSlide(!slide)}
          >
            <span className="h-[2px] bg-white w-full"></span>
            <span className="h-[2px] bg-white w-full"></span>
            <span className="h-[2px] bg-white w-full"></span>
          </div>
        </div>
        <ul
          className={`lg:flex flex-col lg:flex-row justify-between w-1/3 ${
            slide ? "flex" : "hidden"
          } `}
        >
          {navlists.map((navlist) => (
            <li
              key={navlist.path}
              className={`py-2 ${
                pathname == navlist.path
                  ? "font-bold border border-0  border-b-2 border-b-white"
                  : null
              }`}
            >
              <Link to={`${navlist.path}`}>{navlist.name}</Link>
            </li>
          ))}
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
